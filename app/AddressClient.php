<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressClient extends Model
{
    protected $guarded = [];
    protected $table = 'address_client';

    public function shipments(){
        return $this->hasMany(Shipment::class,'client_address');
    }
}
