<?php

namespace App\Services;

use App\Shipment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Auth;

class ShipmentsExport implements FromCollection,WithHeadings {
  public $branch_id;
  public $client_id;
  public $type;
  public $status;
 
  
  public function styles(Worksheet $sheet)
  {
      return [
          // Style the first row as bold text.
          1    => ['font' => ['bold' => true]],
      ];
  }

  public function headings(): array
  {
      if(Auth::user()->user_type == 'customer'){
          return ['ID',"Code", "Type", "Status",  "Shipping Date", "Client Address", "Client Phone", "Reciver Name", "Reciver Phone", "Reciver Address" ,'From State','To State','From Area','To Area' , "Payment Type",  "Shipping Cost", "Amount To Be Collected", 'Created At', "Link"];
      }elseif(Auth::user()->user_type == 'branch'){
          return ['ID',"Code", "Type", "Status", "Client", "Shipping Date", "Client Address", "Client Phone", "Reciver Name", "Reciver Phone", "Reciver Address" ,'From State','To State','From Area','To Area' , "Payment Type",  "Shipping Cost",  "Amount To Be Collected", 'Created At', "Link"];
      }else {        
          return ['ID',"Code", "Type", "Status",  "Client", "Shipping Date", "Client Address", "Client Phone", "Reciver Name", "Reciver Phone", "Reciver Address" ,'From State','To State','From Area','To Area' , "Payment Type", "Shipping Cost", "Amount To Be Collected",'Created At', "Link"];
      }
  }
  /**
  * @return \Illuminate\Support\Collection
  */
  public function collection() {
     return collect(Shipment::getShipmentsReport($this->branch_id,$this->client_id,$this->type,$this->status));
  }
}