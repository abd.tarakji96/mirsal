@extends('backend.layouts.form')

@section('sub_title'){{translate('Create New Shipment')}}@endsection

@section('content')

@php
    $branchs = \App\Branch::where('is_archived', 0)->get();
    $countries = \App\Country::where('covered',1)->get();
    $packages = \App\Package::all();
    $deliveryTimes = \App\DeliveryTime::all();

    $is_def_mile_or_fees = \App\ShipmentSetting::getVal('is_def_mile_or_fees');
    // is_def_mile_or_fees if result 1 for mile if result 2 for fees

    $checked_google_map = \App\BusinessSetting::where('type', 'google_map')->first();

    if(!$is_def_mile_or_fees){
        $is_def_mile_or_fees = 0;
    }
@endphp
<style>
    label {
        font-weight: bold !important;
    }

    .select2-container {
        display: block !important;
    }
    .lang-switch{
        position: absolute;
        top:0;
        left:0;
    }
    .lang-switch a{
        color:black!important;
        text-decoration: underline!important;
    }
    .g-recaptcha div{
        margin: auto;
    }
</style>
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="mx-auto container" style="position: relative;">
    <div class="lang-switch" style="margin: 25px;"><a href="{{ route('client.shipments.create',['lang'=>["ar"=>"en","en"=>"ar"][app()->getLocale()]]) }}">{{ ["ar"=>"English","en"=>"Arabic"][app()->getLocale()] }}</a></div>
    @if(get_setting('system_logo_black') != null)
        <img src="{{ uploaded_asset(get_setting('system_logo_black')) }}" alt="{{ get_setting('site_name') }}" class="d-block" style="margin:35px auto;width:250px;">
    @else
        <img src="{{ static_asset('assets/img/logo.svg') }}" alt="{{ get_setting('site_name') }}" class="d-block" style="margin:35px auto;">
    @endif
    <div class="mb-2">{{translate('For urgent shipment contact us on Whatsapp')}} <a href="https://wa.me/+971508025147" target="_blank" class="fab fa-whatsapp" aria-hidden="true" style="position: relative;top:4px;font-size:22px;color:#4ec25a;font-weight: 900;"></a></div>
    <div class="card mb-20">
        <div class="card-header">
            <h5 class="mb-0 h6">{{translate('Shipment Info')}}</h5>
        </div>

        @if( \App\ShipmentSetting::getVal('def_shipping_cost') == null || count($countries) == 0 || \App\State::where('covered', 1)->count() == 0 || \App\Area::count() == 0 || count($packages) == 0 || $branchs->count() == 0)
            <div class="row">
                <div class="text-center alert alert-danger col-lg-8" style="margin: auto;margin-top:10px;" role="alert">
                    {{translate('Please ask your administrator to configure shipment settings first, before you can create a new shipment!')}}
                </div>
            </div>
        @endif

        <form class="form-horizontal" action="{{ route('client.shipments.store') }}" id="kt_form_1" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="Shipment[type]" value="{{ \App\ShipmentSetting::getVal('def_shipment_type') }}"/>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-md-6 d-none">
                                <div class="form-group">
                                    <label>{{translate('Branch')}}:</label>
                                    <select class="form-control kt-select2 select-branch" name="Shipment[branch_id]">
                                        <option></option>
                                        @foreach($branchs as $branch)
                                            <option @if(\App\ShipmentSetting::getVal('def_branch')==$branch->id) selected @endif value="{{$branch->id}}">{{$branch->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                @if(\App\ShipmentSetting::getVal('is_date_required') == '1' || \App\ShipmentSetting::getVal('is_date_required') == null)
                                <div class="form-group">
                                    <label>{{translate('Shipping Date')}}:</label>
                                    <div class="input-group date">
                                        @php
                                            $defult_shipping_date = \App\ShipmentSetting::getVal('def_shipping_date');
                                            if($defult_shipping_date == null )
                                            {
                                                $shipping_data = \Carbon\Carbon::now()->addDays(0);
                                            }else{
                                                $shipping_data = \Carbon\Carbon::now()->addDays($defult_shipping_date);
                                            }
                                        @endphp
                                        <input type="text" placeholder="{{translate('Shipping Date')}}" value="{{ $shipping_data->toDateString() }}" name="Shipment[shipping_date]" autocomplete="off" class="form-control" id="kt_datepicker_3" />
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Sender Name')}}:</label>
                                    <input type="text" placeholder="{{translate('Sender Name')}}" name="Client[name]" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Sender Phone')}}:</label>
                                    <input type="text" placeholder="{{translate('Sender Phone')}}" name="Client[phone]" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-6" style="display: none;">
                                <div class="form-group">
                                    <label>{{translate('Sender Country')}}:</label>
                                    <select id="change-country" name="Shipment[from_country_id]" class="form-control select-country">
                                        <option value=""></option>
                                        @foreach($countries as $country)
                                        <option value="{{$country->id}}" @if($country->id==231) selected @endif>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Sender Region')}}:</label>
                                    <select id="change-state-from" name="Shipment[from_state_id]" class="form-control select-state">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Sender Area')}}:</label>
                                    <select name="Shipment[from_area_id]" id="from_area_id" class="form-control select-area">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>{{translate('Sender Address')}}:</label>
                                    <input type="text" placeholder="{{translate('Sender Address')}}" name="Client[address]" class="form-control" />
                                </div>
                            </div>
                            @if($checked_google_map->value == 1 )
                                <div class="location-client">
                                    <label>{{translate('Sender Location')}}:</label>
                                    <input type="text" class="form-control address-client " placeholder="{{translate('Sender Location')}}" name="sender_street_address_map"  rel="client" value="" />
                                    <input type="hidden" class="form-control lat" data-client="lat" name="sender_lat" />
                                    <input type="hidden" class="form-control lng" data-client="lng" name="sender_lng" />
                                    <input type="hidden" class="form-control url" data-client="url" name="sender_url" />

                                    <div class="mt-2 col-sm-12 map_canvas map-client" style="width:100%;height:300px;"></div>
                                    <span class="form-text text-muted">{{'Change the pin to select the right location'}}</span>
                                </div>
                            @endif
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Receiver Name')}}:</label>
                                    <input type="text" placeholder="{{translate('Receiver Name')}}" name="Shipment[reciver_name]" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Receiver Phone')}}:</label>
                                    <input type="text" placeholder="{{translate('Receiver Phone')}}" name="Shipment[reciver_phone]" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-6" style="display: none;">
                                <div class="form-group">
                                    <label>{{translate('Receiver Country')}}:</label>
                                    <select id="change-country-to" name="Shipment[to_country_id]" class="form-control select-country">
                                        <option value=""></option>
                                        @foreach($countries as $country)
                                        <option value="{{$country->id}}" @if($country->id==231) selected @endif>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Receiver Region')}}:</label>
                                    <select id="change-state-to" name="Shipment[to_state_id]" class="form-control select-state">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Receiver Area')}}:</label>
                                    <select name="Shipment[to_area_id]" class="form-control select-area">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>{{translate('Receiver Address')}}:</label>
                                    <input type="text" placeholder="{{translate('Receiver Address')}}" name="Shipment[reciver_address]" class="form-control" />
                                </div>
                            </div>
                            @if($checked_google_map->value == 1 )
                                <div class="col-md-12">
                                    <div class="location-receiver">
                                        <label>{{translate('Receiver Location')}}:</label>
                                        <input type="text" class="form-control address-receiver " placeholder="{{translate('Receiver Location')}}" name="Shipment[reciver_street_address_map]"  rel="receiver" value="" />
                                        <input type="hidden" class="form-control lat" data-receiver="lat" name="Shipment[reciver_lat]" />
                                        <input type="hidden" class="form-control lng" data-receiver="lng" name="Shipment[reciver_lng]" />
                                        <input type="hidden" class="form-control url" data-receiver="url" name="Shipment[reciver_url]" />

                                        <div class="mt-2 col-sm-12 map_canvas map-receiver" style="width:100%;height:300px;"></div>
                                        <span class="form-text text-muted">{{'Change the pin to select the right location'}}</span>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Payment Type')}}:</label>
                                    <select class="form-control kt-select2 payment-type" id="payment_type" name="Shipment[payment_type]">
                                        <option @if(\App\ShipmentSetting::getVal('def_payment_type')=='1' ) selected @endif value="1">{{translate('Postpaid')}}</option>
                                        <option @if(\App\ShipmentSetting::getVal('def_payment_type')=='2' ) selected @endif value="2">{{translate('Prepaid')}}</option>
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Payment Method')}}:</label>
                                    <select class="form-control kt-select2 payment-method" id="payment_method_id" name="Shipment[payment_method_id]">
                                        @forelse (\App\BusinessSetting::where("key","payment_gateway")->where("value","1")->get() as $gateway)
                                            <option value="{{$gateway->id}}" @if($gateway->id == 11) selected @endif>{{$gateway->name}}</option>
                                        @empty
                                            <option value="11">{{translate('Cash')}}</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="display:none">
                            <div class="col-md-6">
                                <div class="form-group" >
                                    <label>{{translate('Order ID')}}:</label>
                                    <input type="text" placeholder="{{translate('Order ID')}}" name="Shipment[order_id]" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Attachments')}}:</label>
                                    <div class="input-group " data-toggle="aizuploader" data-type="image" data-multiple="true">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-soft-secondary font-weight-medium">{{ translate('Browse') }}</div>
                                        </div>
                                        <div class="form-control file-amount">{{ translate('Choose File') }}</div>
                                        <input type="hidden" name="Shipment[attachments_before_shipping]" class="selected-files" value="{{old('Shipment[attachments_before_shipping]')}}" max="3">
                                    </div>
                                    <div class="file-preview">
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Attachments After Shipping')}}:</label>

                                    <div class="input-group " data-toggle="aizuploader" data-type="image" data-multiple="true">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-soft-secondary font-weight-medium">{{ translate('Browse') }}</div>
                                        </div>
                                        <div class="form-control file-amount">{{ translate('Choose File') }}</div>
                                        <input type="hidden" name="Shipment[attachments_after_shipping]" class="selected-files" value="{{old('Shipment[attachments_after_shipping]')}}" max="3">
                                    </div>
                                    <div class="file-preview">
                                    </div>
                                </div>
                            </div> --}}
                        </div>

                        <hr>

                        <div id="kt_repeater_1">
                            <div class="row" id="kt_repeater_1">
                                <h2 class="text-left d-flex" style="justify-content: space-between;width: 100%;">
                                    <div>
                                        {{translate('Shipment Info')}}:
                                    </div>
                                    <div class="mx-8" style="display:none;">
                                        <a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary">
                                            <i class="la la-plus"></i>{{translate('Add')}}
                                        </a>
                                    </div>
                                </h2>

                                <div data-repeater-list="Package" class="col-lg-12">
                                    <div data-repeater-item class="row align-items-center my-8" style="padding-bottom: 15px;padding-top: 15px;border:1px solid #ccc;">
                                        <div class="col-md-3">
                                            <label>{{translate('Delivery Time')}}:</label>
                                            <select id="package_id" class="form-control kt-select2 package-type-select" name="package_id">
                                                <option></option>
                                                @foreach($packages as $package)
                                                <option @if(\App\ShipmentSetting::getVal('def_package_type')==$package->id) selected @endif value="{{$package->id}}">{{$package->name}}</option>
                                                @endforeach
                                            </select>
                                            <div class="mb-2 d-md-none"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <label>{{translate('Cost')}}:</label>
                                            <input id="cost" type="text" class="form-control" name="cost" value="50 AED" disabled>
                                            <div class="mb-2 d-md-none"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <label>{{translate('description')}}:</label>
                                            <input type="text" placeholder="{{translate('description')}}" class="form-control" name="description" value="" required>
                                            <div class="mb-2 d-md-none"></div>
                                        </div>
                                        <div class="col-md-3">

                                            <label>{{translate('Quantity')}}:</label>

                                            <input class="kt_touchspin_qty" placeholder="{{translate('Quantity')}}" type="number" min="1" name="qty" class="form-control" value="1" />
                                            <div class="mb-2 d-md-none"></div>

                                        </div>
                                        <div class="col-md-3" style="display: none;">

                                            <label>{{translate('Weight')}}:</label>

                                            <input type="number" min="1" placeholder="{{translate('Weight')}}" name="weight" class="form-control weight-listener kt_touchspin_weight" onchange="calcTotalWeight()" value="1" />
                                            <div class="mb-2 d-md-none"></div>

                                        </div>

                                        <div class="col-md-12" style="margin-top: 10px;display: none;">
                                            <label>{{translate('Dimensions [Length x Width x Height] (cm):')}}:</label>
                                        </div>
                                        <div class="col-md-2" style="display: none;">

                                            <input class="dimensions_r" type="number" min="1" class="form-control" placeholder="{{translate('Length')}}" name="length" value="1" />

                                        </div>
                                        <div class="col-md-2" style="display: none;">

                                            <input class="dimensions_r" type="number" min="1" class="form-control" placeholder="{{translate('Width')}}" name="width" value="1" />

                                        </div>
                                        <div class="col-md-2" style="display: none;">

                                            <input class="dimensions_r" type="number" min="1" class="form-control " placeholder="{{translate('Height')}}" name="height" value="1" />

                                        </div>
                                        <div class="row" style="padding-top: 30px; display:none;">
                                            <div class="col-md-12">
                                                <div>
                                                    <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger delete_item">
                                                        <i class="la la-trash-o"></i>{{translate('Delete')}}
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{translate('Amount to be Collected')}}:</label>
                                        <input id="kt_touchspin_3" placeholder="{{translate('Amount to be Collected')}}" type="text" min="0" class="form-control" value="0" name="Shipment[amount_to_be_collected]" />
                                    </div>
                                </div>
                                <div class="col-md-6" style="display:none;">
                                    <div class="form-group">
                                        <label>{{translate('Delivery Time')}}:</label>
										<select class="form-control kt-select2 delivery-time" disabled>
											<option value="24hours">24 hours</option>
                                        </select>

                                    </div>
                                </div>
								  <div class="col-md-6" style="display:none;">
                                    <div class="form-group">
                                        <label>{{translate('Delivery Time')}}:</label>
										<select class="form-control kt-select2 delivery-time" id="delivery_time" name="Shipment[delivery_time]">
											<option value="24hours">24hours</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6" style="display: none;">
                                    <div class="form-group">
                                        <label>{{translate('Total Weight')}}:</label>
                                        <input id="kt_touchspin_4" placeholder="{{translate('Total Weight')}}" type="text" min="1" class="form-control total-weight" value="1" name="Shipment[total_weight]" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {!! hookView('shipment_addon',$currentView) !!}

                    @if(config('services.recaptcha.key'))
                        <div class="g-recaptcha col-lg-12 mb-6"
                            data-sitekey="{{config('services.recaptcha.key')}}">
                        </div>
                    @endif
                    <div class="mb-0 mx-auto text-right form-group">
                        <button type="button" class="submit-btn btn btn-sm btn-primary px-20" onclick="get_estimation_cost()">{{translate('Save')}}</button>

                        <!-- Button trigger modal -->
                        <button type="button" class="submit-btn btn btn-sm btn-primary d-none px-20" data-toggle="modal" data-target="#exampleModalCenter" id="modal_open">
                            {{translate('Save')}}
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">{{translate('Estimation Cost')}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="modal_close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="text-left modal-body">
                                        <div class="row">
                                            @if($is_def_mile_or_fees=='2')
                                                <div class="col-6">{{translate('Shipping Cost')}} :</div>
                                                <div class="col-6" id="shipping_cost"></div>
                                            @elseif($is_def_mile_or_fees=='1')
                                                <div class="col-6">{{translate('Mile Cost')}} :</div>
                                                <div class="col-6" id="mile_cost"></div>
                                            @endif
                                        </div>
                                        <div class="row">
                                            <div class="col-6">{{translate('Tax & Duty')}} :</div>
                                            <div class="col-6" id="tax_duty"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">{{translate('Insurance')}} :</div>
                                            <div class="col-6" id="insurance"></div>
                                        </div>
                                        <div class="row">
                                            @if(\App\ShipmentSetting::getVal('is_def_mile_or_fees')=='2')
                                                <div class="col-6">{{translate('Return Cost')}} :</div>
                                                <div class="col-6" id="return_cost"></div>
                                            @elseif(\App\ShipmentSetting::getVal('is_def_mile_or_fees')=='1')
                                                <div class="col-6">{{translate('Return Mile Cost')}} :</div>
                                                <div class="col-6" id="return_mile_cost"></div>
                                            @endif
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-6">{{translate('TOTAL COST')}} :</div>
                                            <div class="col-6" id="total_cost"></div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{translate('Close')}}</button>
                                        <button type="submit" class="btn btn-primary"  data-dismiss="modal">{{translate('Confirm Shipment')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </form>

    </div>
</div>

@endsection

@section('script')
<script src="{{ static_asset('assets/dashboard/js/geocomplete/jquery.geocomplete.js') }}"></script>
<script src="//maps.googleapis.com/maps/api/js?libraries=places&key={{$checked_google_map->key}}"></script>

<script type="text/javascript">

    let state_to_change = null;
    let area_to_change = null;
    let fv = null;

    // Map Address For Receiver
    $('.address-receiver').each(function(){
        var address = $(this);
        address.geocomplete({
            map: ".map_canvas.map-receiver",
            mapOptions: {
                zoom: 8,
                center: { lat: -34.397, lng: 150.644 },
            },
            markerOptions: {
                draggable: true
            },
            details: ".location-receiver",
            detailsAttribute: 'data-receiver',
            autoselect: true,
            restoreValueAfterBlur: true,
        });
        address.bind("geocode:dragged", function(event, latLng){
            $("input[data-receiver=lat]").val(latLng.lat());
            $("input[data-receiver=lng]").val(latLng.lng());
        });
    });

    // Map Address For Client
    $('.address-client').each(function(){
        var address = $(this);
        address.geocomplete({
            map: ".map_canvas.map-client",
            mapOptions: {
                zoom: 8,
                center: { lat: -34.397, lng: 150.644 },
            },
            markerOptions: {
                draggable: true
            },
            details: ".location-client",
            detailsAttribute: 'data-client',
            autoselect: true,
            restoreValueAfterBlur: true,
        });
        address.bind("geocode:dragged", function(event, latLng){
            $("input[data-client=lat]").val(latLng.lat());
            $("input[data-client=lng]").val(latLng.lng());
        });
    });

    var inputs = document.getElementsByTagName('input');

    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].type.toLowerCase() == 'number') {
            inputs[i].onkeydown = function(e) {
                if (!((e.keyCode > 95 && e.keyCode < 106) ||
                        (e.keyCode > 47 && e.keyCode < 58) ||
                        e.keyCode == 8)) {
                    return false;
                }
            }
        }
    }

    $('.payment-method').select2({
        placeholder: "Payment Method",
    });

    $('.payment-type').select2({
        placeholder: "Payment Type",
    });

    $('.select-branch').select2({
            placeholder: "Select Branch",
    })

    $('#change-country-client-address').change(function() {
        var id = $(this).val();
        $.get("{{route('admin.shipments.get-states-ajax')}}?country_id=" + id, function(data) {
            $('select[name ="state_id"]').empty();
            $('select[name ="state_id"]').append('<option value=""></option>');
            for (let index = 0; index < data.length; index++) {
                const element = data[index];

                $('select[name ="state_id"]').append('<option value="' + element['id'] + '">' + element['name'] + '</option>');
            }


        });
    });
    $('#change-country').change(function() {
        var id = $(this).val();
        $.get("{{route('admin.shipments.get-states-ajax')}}?country_id=" + id, function(data) {
            $('select[name ="Shipment[from_state_id]"]').empty();
            $('select[name ="Shipment[from_state_id]"]').append('<option value=""></option>');
            for (let index = 0; index < data.length; index++) {
                const element = data[index];

                $('select[name ="Shipment[from_state_id]"]').append('<option value="' + element['id'] + '">' + element['name'] + '</option>');
            }
            if(state_to_change)
            {
                $("#change-state-from").val(state_to_change).change();
                state_to_change = null;
            }
        });
    });

    $('#change-country-to').change(function() {
        var id = $(this).val();

        $.get("{{route('admin.shipments.get-states-ajax')}}?country_id=" + id, function(data) {
            $('select[name ="Shipment[to_state_id]"]').empty();
            $('select[name ="Shipment[to_state_id]"]').append('<option value=""></option>');
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                $('select[name ="Shipment[to_state_id]"]').append('<option value="' + element['id'] + '">' + element['name'] + '</option>');
            }


        });
    });

    $('#change-state-from').change(function() {
        var id = $(this).val();

        $.get("{{route('admin.shipments.get-areas-ajax')}}?state_id=" + id, function(data) {
            $('select[name ="Shipment[from_area_id]"]').empty();
            $('select[name ="Shipment[from_area_id]"]').append('<option value=""></option>');
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                $('select[name ="Shipment[from_area_id]"]').append('<option value="' + element['id'] + '">' + element['name'] + '</option>');
            }

            if(area_to_change)
            {
                $("#from_area_id").val(area_to_change).change();
                area_to_change = null;
            }
        });
    });
    $('#change-state-to').change(function() {
        var id = $(this).val();

        $.get("{{route('admin.shipments.get-areas-ajax')}}?state_id=" + id, function(data) {
            $('select[name ="Shipment[to_area_id]"]').empty();
            $('select[name ="Shipment[to_area_id]"]').append('<option value=""></option>');
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                $('select[name ="Shipment[to_area_id]"]').append('<option value="' + element['id'] + '">' + element['name'] + '</option>');
            }


        });
    });

    function get_estimation_cost() {
        var total_weight = document.getElementById('kt_touchspin_4').value;
        var select_packages = document.getElementsByClassName('package-type-select');

        var from_country_id = document.getElementsByName("Shipment[from_country_id]")[0].value;
        var to_country_id = document.getElementsByName("Shipment[to_country_id]")[0].value;
        var from_state_id = document.getElementsByName("Shipment[from_state_id]")[0].value;
        var to_state_id = document.getElementsByName("Shipment[to_state_id]")[0].value;
        var from_area_id = document.getElementsByName("Shipment[from_area_id]")[0].value;
        var to_area_id = document.getElementsByName("Shipment[to_area_id]")[0].value;
        var client_id = 0;

        var package_ids = [];
        for (let index = 0; index < select_packages.length; index++) {
            if(select_packages[index].value){
                package_ids[index] = new Object();
                package_ids[index]["package_id"] = select_packages[index].value;
            }else{
                AIZ.plugins.notify('danger', '{{ translate('Please select package type') }} ' + (index+1));
                return 0;
            }
        }
        var request_data = { _token : '{{ csrf_token() }}',
                                package_ids : package_ids,
                                total_weight : total_weight,
                                from_country_id : from_country_id,
                                to_country_id : to_country_id,
                                from_state_id : from_state_id,
                                to_state_id : to_state_id,
                                from_area_id : from_area_id,
                                to_area_id : to_area_id,
                                client_id : client_id,
                            };
        $.post('{{ route('admin.shipments.get-estimation-cost') }}', request_data, function(response){

            if({{$is_def_mile_or_fees}} =='2')
            {
                document.getElementById("shipping_cost").innerHTML = response.shipping_cost;
                document.getElementById("return_cost").innerHTML = response.return_cost;
            }else if({{$is_def_mile_or_fees}} =='1')
            {
                document.getElementById("mile_cost").innerHTML = response.shipping_cost;
                document.getElementById("return_mile_cost").innerHTML = response.return_cost;
            }
            document.getElementById("tax_duty").innerHTML = response.tax;
            document.getElementById("insurance").innerHTML = response.insurance;
            document.getElementById("total_cost").innerHTML = response.total_cost;
            document.getElementById('modal_open').click();
            console.log(response);
        });
    }

    function calcTotalWeight() {
        console.log('sds');
        var elements = $('.weight-listener');
        var sumWeight = 0;
        elements.map(function() {
            sumWeight += parseInt($(this).val());
            console.log(sumWeight);
        }).get();
        $('.total-weight').val(sumWeight);
    }

    $(document).ready(function() {

        $('.select-country').select2({
            placeholder: "Select country",
            language: {
              noResults: function() {
                    return ``;
              },
            },
            escapeMarkup: function(markup) {
              return markup;
            },
        });

        $('.select-state').select2({
            placeholder: "Select state",
            language: {
              noResults: function() {
                    return ``;
              },
            },
            escapeMarkup: function(markup) {
              return markup;
            },
        });

        $('.select-address').select2({
            placeholder: "Select Client First",
        })

        $('.select-area').select2({
            placeholder: "Select Area",
            language: {
              noResults: function() {
                    return ``;
              },
            },
            escapeMarkup: function(markup) {
              return markup;
            },
        });

        $('.select-country').trigger('change');
        $('.select-state').trigger('change');

        $('#kt_datepicker_3').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayBtn: true,
            todayHighlight: true,
            startDate: new Date(),
        });
        $( document ).ready(function() {
            $('.package-type-select').select2({
                placeholder: "Package Type",
                language: {
                noResults: function() {
                        return ``;
                },
                },
                escapeMarkup: function(markup) {
                return markup;
                },
            });
        });


        //Package Types Repeater

        $('#kt_repeater_1').repeater({
            initEmpty: false,

            show: function() {
                $(this).slideDown();

                $('.package-type-select').select2({
                    placeholder: "Package Type",
                    language: {
                    noResults: function() {
                            return ``;
                    },
                    },
                    escapeMarkup: function(markup) {
                    return markup;
                    },
                });

                $('.dimensions_r').TouchSpin({
                    buttondown_class: 'btn btn-secondary',
                    buttonup_class: 'btn btn-secondary',

                    min: 1,
                    max: 1000000000,
                    stepinterval: 50,
                    maxboostedstep: 10000000,
                    initval: 1,
                });

                $('.kt_touchspin_weight').TouchSpin({
                    buttondown_class: 'btn btn-secondary',
                    buttonup_class: 'btn btn-secondary',

                    min: 1,
                    max: 1000000000,
                    stepinterval: 50,
                    maxboostedstep: 10000000,
                    initval: 1,
                    prefix: 'Kg'
                });
                $('.kt_touchspin_qty').TouchSpin({
                    buttondown_class: 'btn btn-secondary',
                    buttonup_class: 'btn btn-secondary',

                    min: 1,
                    max: 1000000000,
                    stepinterval: 50,
                    maxboostedstep: 10000000,
                    initval: 1,
                });
                calcTotalWeight();
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
		$('#package_id').change(function(){
		var name = $(this).val();
		var delivery_time1 = document.getElementsByClassName("delivery-time")[0][0];
		var delivery_time2 = document.getElementsByClassName("delivery-time")[1][0];
        var cost = document.getElementById("cost");

		if(name == '1'){
			delivery_time1.value = "24hours";
			delivery_time1.text = "24 hours";
            cost.value = "50 AED";
			delivery_time2.value = "24hours";
			delivery_time2.text = "24 hours";
		}else if(name == '3'){
			delivery_time1.value = "12hours";
			delivery_time1.text = "12 hours";
            cost.value = "100 AED";

			delivery_time2.value = "12hours";
			delivery_time2.text = "12 hours";

		}else if(name == '4'){
			delivery_time1.value = "5hours";
			delivery_time1.text = "5 hours";
            cost.value = "250 AED";

			delivery_time2.value = "5hours";
			delivery_time2.text = "5 hours";
		}else{
			delivery_time1.value = "3hours";
			delivery_time1.text = "3 hours";
            cost.value = "350 AED";

			delivery_time2.value = "3hours";
			delivery_time2.text = "3 hours";
		}


	});


        $('body').on('click', '.delete_item', function(){
            $('.total-weight').val("{{translate('Calculated...')}}");
            setTimeout(function(){ calcTotalWeight(); }, 500);
        });
        $('#kt_touchspin_2, #kt_touchspin_2_2').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',

            min: -1000000000,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '%'
        });
        $('#kt_touchspin_3').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',

            min: 0,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '{{currency_symbol()}}'
        });
        $('#kt_touchspin_4').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',

            min: 1,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            initval: 1,
            prefix: 'Kg'
        });
        $('.kt_touchspin_weight').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',

            min: 1,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            initval: 1,
            prefix: 'Kg'
        });
        $('.kt_touchspin_qty').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',

            min: 1,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            initval: 1,
        });
        $('.dimensions_r').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',

            min: 1,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            initval: 1,
        });


        fv = FormValidation.formValidation(
            document.getElementById('kt_form_1'), {
                fields: {
                    "Shipment[shipping_date]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "description": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "Shipment[branch_id]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "Shipment[payment_type]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "Shipment[payment_method_id]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },

                    "Shipment[delivery_time]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "Shipment[total_weight]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "Shipment[from_country_id]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "Shipment[to_country_id]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "Shipment[from_state_id]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "Shipment[to_state_id]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "Shipment[from_area_id]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "Shipment[to_area_id]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "Shipment[reciver_name]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "Shipment[reciver_phone]": {
                        validators: {
                             notEmpty: {
                                message: '{{translate("This is required!")}}'
                            },
                        
                        }
                    },
                    "Shipment[reciver_address]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "Client[name]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                    "Client[phone]": {
                        validators: {
                             notEmpty: {
                                message: '{{translate("This is required!")}}'
                            },
                           
                        }
                    },
                    "Client[address]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                      "Package[0][description]": {
                        validators: {
                            notEmpty: {
                                message: '{{translate("This is required!")}}'
                            }
                        }
                    },
                },


                plugins: {
                    autoFocus: new FormValidation.plugins.AutoFocus(),
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                    icon: new FormValidation.plugins.Icon({
                        valid: '',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh',
                    }),
					 alias: new FormValidation.plugins.Alias({
                        // The required validator is infact treated as notEmpty validator
                        chackPhoneClient: 'callback',
                        chackPhoneReciver: 'callback',
                    }),
                }
            }
        );
    });
    setInterval(() => {
        var recaptcha = $("#g-recaptcha-response");
        $(".submit-btn").prop('disabled', recaptcha.val() === "");
    }, 500);
</script>
@endsection
