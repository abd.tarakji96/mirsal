@extends('backend.layouts.app')

@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
		<div class="col-md-6">
			<h1 class="h3">{{translate('All Customers')}}</h1>
		</div>
		<div class="col-md-6 text-md-right">
			<a href="{{ route('admin.clients.create') }}" class="btn btn-circle btn-info">
				<span>{{translate('Add New Customers')}}</span>
			</a>
		</div>
	</div>
</div>

<div class="card">
    <div class="card-header">
        <h5 class="mb-0 h6">{{translate('Customers')}}</h5>
    </div>
    <div class="card-body">
		<form method="GET" action="{{url()->current()}}" id="search_form">
            <div class="mb-7">
                <div class="row align-items-center">
					
                    <div class="col-lg-12 col-xl-12 mb-3">
                        <div class="row align-items-center">
							<div class="my-2 col-md-4 my-md-0">
                                <div class="input-icon">
                                    <input type="text" name="search" value="<?php if (isset($_GET['search'])) {
                                                                                echo $_GET['search'];
                                                                            } ?>" class="form-control" placeholder="{{translate('Search')}}" id="kt_datatable_search_query" />
                                    <span>
                                        <i class="flaticon2-search-1 text-muted"></i>
                                    </span>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <div class="mt-5 col-lg-3 col-xl-12 mt-lg-0 d-flex">
                        <div>
                            <button type="submit" class="px-6 btn btn-light-primary font-weight-bold">{{translate('Search')}}</button>
                        </div>
                    </div>
                </div>
				</div>
        </form>
       <div class="table-responsive">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th  width="3%">#</th>
                    <th >{{translate('Name')}}</th>
                    <th >{{translate('Email')}}</th>
                    <th >{{translate('Phone')}}</th>
                    <th >{{translate('Wallet')}}</th>
                    
                    <th  width="10%" class="text-center">{{translate('Options')}}</th>
                    <th>{{translate('Accounting')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($clients as $key => $client)
                    @php
                        $client_tran = App\Transaction::where('client_id' , $client->id)->sum('value');
                    @endphp
                        <tr>
                            <td  width="3%">{{ ($key+1) + ($clients->currentPage() - 1)*$clients->perPage() }}</td>
                            <td width="10%">{{$client->name}}</td>
                            <td width="15%">{{$client->email}}</td>
                            <td width="15%">{{$client->responsible_mobile}}</td>
                            <td> {{ $client_tran }}</td>
                            <td class="text-center">
                                    <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('admin.clients.show', $client->id)}}" title="{{ translate('Show') }}">
		                                <i class="las la-eye"></i>
		                            </a>
		                            <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('admin.clients.edit', $client->id)}}" title="{{ translate('Edit') }}">
		                                <i class="las la-edit"></i>
		                            </a>
		                            <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('admin.clients.delete-client', ['client'=>$client->id])}}" title="{{ translate('Delete') }}">
		                                <i class="las la-trash"></i>
		                            </a>
		                        </td>
                                <td>
                                      
                                        <button type="button" class="btn btn-success btn-sm"
                                        data-toggle="modal" data-target="#accounting"
                                        onclick="set_client_id({{ $client->id }},{{ $client_tran }})"
                                        title="{{ translate('Accounting') }}">
                                        {{ translate('Accounting') }}
                                    </button>
                                </td>
                        </tr>

                @endforeach
            </tbody>
        </table>
        </div>
        <div class="aiz-pagination">
            {{ $clients->appends(request()->input())->links() }}
        </div>
    </div>
</div>
{!! hookView('spot-cargo-shipment-client-addon',$currentView) !!}

<div class="modal fade" id="accounting" tabindex="-1" role="dialog" aria-labelledby="accountingModalLabel"
            aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="accountingModalLabel">{{ translate('Accounting') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        
                        <div class="modal-body">
                            <h5 class="mb-2 modal-title" id="accountingModalLabel">{{ translate('When you click Confirm, the client wallet will become empty') }}</h5>

                        </div>
        
                        <div class="modal-footer">
                            <form action="{{ route('admin.client.accounting') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="client_id" id="selected_client_id" />
                                <input type="hidden" name="amount" id="amount" />
                                <button type="button" class="btn btn-secondary"
                                    data-dismiss="modal">{{ translate('Close') }}</button>
                                <button type="submit" class="btn btn-primary">{{ translate('Confirm') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection

<script>

    function set_client_id(client_id,amount) {

        document.getElementById("selected_client_id").value = client_id;
        document.getElementById("amount").value = amount;
    }
</script>