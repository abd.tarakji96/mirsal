<style>
    .card-more-info{
        text-align:center;
        padding:5px;
        cursor: pointer;
        background-color: #0002;
    }
    .card-icon {
        font-size: 60px;
        color: #0002;
    }
    .card-spacer {
        padding: 1rem 1.25rem !important;
    }
    .dashcard.bg-primary{
        background-color: #dc3545 !important;
        color: white;
    }
    .dashcard.bg-primary .card-more-info i{
        color: white;
        font-size: 15px;
    }
    .dashcard.bg-warning .card-more-info i{
        color: black;
        font-size: 15px;
    }
    .dashcard.bg-success{
        background-color: #3bd4c2 !important;
        color: white;
    }
    .dashcard.bg-success .card-more-info i{
        color: white;
        font-size: 15px;
    }
    .dashcard.bg-dark{
        background-color: #04AA6D !important;
        color: white;
    }
    .dashcard.bg-dark .card-more-info i{
        color: white;
        font-size: 15px;
    }

</style>
@php
    $user_type = Auth::user()->user_type;
    $staff_permission = json_decode(Auth::user()->staff->role->permissions ?? "[]");
@endphp

@if($user_type == 'admin' || in_array('1100', $staff_permission) || in_array('1008', $staff_permission) || in_array('1009', $staff_permission) )
    @php
        $all_clients         = App\Client::where('is_archived',0)->whereRaw('email <> ""')->count();

        $all_shipments            = App\Shipment::count();
        $pending_shipments        = 0;//App\Shipment::whereIn('status_id', [App\Shipment::REQUESTED_STATUS, App\Shipment::CAPTAIN_ASSIGNED_STATUS, App\Shipment::RECIVED_STATUS, App\Shipment::RETURNED_STOCK])->count();
        $delivered_shipments      = App\Shipment::whereIn('status_id', [App\Shipment::DELIVERED_STATUS, App\Shipment::SUPPLIED_STATUS, App\Shipment::RETURNED_CLIENT_GIVEN])->count();
        $returned_shipments       = 0;//App\Shipment::where('status_id', App\Shipment::RETURNED_CLIENT_GIVEN)->count();
        $saved_pickup_shipments   = App\Shipment::where('status_id', App\Shipment::SAVED_STATUS)->where('type',App\Shipment::PICKUP)->count();
        $saved_delivery_shipments = App\Shipment::where('status_id', App\Shipment::SAVED_STATUS)->where('type',App\Shipment::DROPOFF)->count();
        $approved_shipments       = App\Shipment::where('status_id', App\Shipment::APPROVED_STATUS)->count();

        $all_missions      = 0;//App\Mission::count();
        $pending_missions  =  App\Mission::whereIn('status_id',[ App\Mission::REQUESTED_STATUS])->count();
        $pickup_missions   = 0;//App\Mission::where('type', App\Mission::PICKUP_TYPE )->count();
        $approved_missions = App\Mission::where('status_id', App\Mission::APPROVED_STATUS)->count();
        $recived_missions  = App\Mission::where('status_id', App\Mission::RECIVED_STATUS)->count();
        $done_missions     = App\Mission::where('status_id', App\Mission::DONE_STATUS )->count();
        $delivery_missions = 0;//App\Mission::where('type', App\Mission::DELIVERY_TYPE )->count();
        $transfer_missions = 0;//App\Mission::where('type', App\Mission::TRANSFER_TYPE )->count();
        $supply_missions   = 0;//App\Mission::where('type', App\Mission::SUPPLY_TYPE )->count();
        $closed_missions   = App\Mission::where('type', App\Mission::CLOSED_STATUS )->count();


        $delivered_shipments1 = App\Shipment::whereIn('status_id', [App\Shipment::DELIVERED_STATUS])->count();
        $supplied_shipments = App\Shipment::whereIn('status_id', [ App\Shipment::SUPPLIED_STATUS])->count();
		$closed_shipments = App\Shipment::whereIn('status_id', [ App\Shipment::CLOSED_STATUS])->count();
        $returned_on_sender_shipments = App\Shipment::whereIn('status_id', [App\Shipment::RETURNED_STATUS])->count();

    @endphp

    <div class="row">
    {{-- Admin With All Permission And Admin With Shipment Index Permission  --}}
    @if($user_type == 'admin' || in_array('1100', $staff_permission) || in_array('1009', $staff_permission) )
        <div class="col-xl-6"> <!-- Notifications -->
            <!--begin::Stats Widget 30-->
       {{--     <div class="card card-custom card-stretch gutter-b dashcard">
                <!--begin::Body-->
                <div class="d-flex" style="max-height: 200px;">
                    @if (\Auth::user()->unreadNotifications->count() > 0)
                        <!--begin::Nav-->
                        <div class="navi navi-hover scroll mb-4" data-scroll="true" style="flex-grow: 1;"
                            data-height="200" data-mobile-height="200">



                            @foreach (\Auth::user()->unreadNotifications->take(20) as $key => $item)
                                <!--begin::Item-->
                                <a href="{{ route('notification.view', ['id'=>$item->id] ) }}" class="navi-item">
                                    <div class="navi-link">
                                        <div class="navi-icon mr-2">
                                            <i class="@if ($item->icon) {{$item->icon}} @else flaticon2-bell-4 @endif text-success"></i>
                                        </div>
                                        <div class="navi-text">
                                            <div class="font-weight-bold">{{$item->data['message']['subject']}}</div>
                                            <div class="text-muted">{{$item->created_at->diffForHumans(null, null, true)}}</div>
                                        </div>
                                    </div>
                                </a>


                                <!--end::Item-->
                            @endforeach

                        </div>

                        <!--end::Scroll-->

                        <!--begin::Action-->
                        <div class="d-flex flex-center pr-7 ml-4">
                            <a href="{{ route('notifications') }}"
                                class="btn btn-light-primary font-weight-bold text-center">{{translate('See
                                All')}}</a>
                        </div>
                        <!--end::Action-->

                    @else
                        <!--begin::Nav-->
                        <div class="d-flex flex-center text-center text-muted min-h-200px">{{translate('All caught up!')}}
                        <br>{{translate('No new notifications')}}.</div>
                        <!--end::Nav-->
                    @endif
                </div>
                <!--end::Body-->
            </div>--}}
            <!--end::Stats Widget 30-->
        </div>
		<div class="col-xl-6"> </div>
        <div class="col-xl-4"> <!-- All Clients -->
            <!--begin::Stats Widget 30-->
            <div class="card card-custom bg-success card-stretch gutter-b dashcard">
                <!--begin::Body-->
                <div class="p-0 card-body d-flex flex-column justify-content-between">
                    <div class="d-flex align-items-center justify-content-between card-spacer">
                        <div class="mr-2 d-flex flex-column">
                            <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$all_clients}}</span>
                            <span class="mt-2 font-weight-bolder">{{translate('All Customers')}}</span>
                        </div>
                        <i class="card-icon fas fa-users"></i>
                    </div>
                    <div onclick="location.href='{{ route('admin.clients.index') }}'" class="card-more-info font-weight-bolder">
                        {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Stats Widget 30-->
        </div>

    @endif

    {{-- Admin With All Permission And Admin With Shipment Index Permission  --}}
    @if($user_type == 'admin' || in_array('1100', $staff_permission) || in_array('1009', $staff_permission) )
            <div class="col-xl-4"> <!-- All Shipments -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom card-stretch gutter-b dashcard bg-success">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$all_shipments}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Total Shipments Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-box-open"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.shipments.index') }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    {{-- <div class="p-0 card-body d-flex flex-column">
                        <div onclick="location.href='{{ route('admin.shipments.index') }}'" class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <a href="#" class="text-light-75 text-hover-primary font-weight-bolder font-size-h5">{{translate('Total Shipments Count')}}</a>
                                <span class="mt-2 text-muted font-weight-bold">{{translate('Count all shipments in the system')}}</span>
                            </div>
                            <span class="symbol symbol-light-primary symbol-45">
                                <span class="symbol-label font-weight-bolder font-size-h6">{{$all_shipments}}</span>
                            </span>
                        </div>
                    </div> --}}
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-4 d-none"> <!-- Pending Shipments Count -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-primary card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$pending_shipments}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Pending Shipments Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-box-open"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.shipments.index' , ['status'=>[ App\Shipment::REQUESTED_STATUS,App\Shipment::CAPTAIN_ASSIGNED_STATUS,App\Shipment::RETURNED_STOCK,App\Shipment::RECIVED_STATUS ] ]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    {{-- <div class="p-0 card-body d-flex flex-column">
                        <div onclick="location.href='{{ route('admin.shipments.index' , ['status'=>[ App\Shipment::REQUESTED_STATUS,App\Shipment::CAPTAIN_ASSIGNED_STATUS,App\Shipment::RETURNED_STOCK,App\Shipment::RECIVED_STATUS ] ]) }}'" class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <a href="#" class="text-dark-75 font-weight-bolder font-size-h5">{{translate('Pending Shipments Count')}}</a>
                                <span class="mt-2 text-dark-75 font-weight-bold">{{translate('All shipments that need an action to be closed')}}</span>
                            </div>
                            <span class="symbol symbol-light-primary symbol-45">
                                <span class="symbol-label font-weight-bolder font-size-h6">{{$pending_shipments}}</span>
                            </span>
                        </div>
                    </div> --}}
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-4"> <!-- Saved Pickup Shipments -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-warning card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$saved_pickup_shipments}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Saved Pickup Shipments')}}</span>
                            </div>
                            <i class="card-icon fas fa-box-open"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.shipments.saved.index',['status'=>App\Shipment::SAVED_STATUS,'type'=>\App\Shipment::PICKUP]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-4"> <!-- Approved Shipments -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-success card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$approved_shipments}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Approved Shipments')}}</span>
                            </div>
                            <i class="card-icon fas fa-box-open"></i>
                        </div>

                        <div onclick="location.href='{{ route('admin.shipments.approved.index',['status'=>App\Shipment::APPROVED_STATUS]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-4"> <!-- Delivered Shipments -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-dark card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$saved_delivery_shipments}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Saved Dropoff Shipments Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-box-open"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.shipments.saved.index',['status'=>App\Shipment::SAVED_STATUS,'type'=>\App\Shipment::DROPOFF]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    {{-- <div class="p-0 card-body d-flex flex-column">
                        <div onclick="location.href='{{ route('admin.shipments.index' , ['status'=>[ App\Shipment::RETURNED_CLIENT_GIVEN,App\Shipment::SUPPLIED_STATUS,App\Shipment::DELIVERED_STATUS ] ]) }}'" class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <a href="#" class="text-dark-75 font-weight-bolder font-size-h5">{{translate('Delivered Shipments Count')}}</a>
                                <span class="mt-2 text-dark-75 font-weight-bold">{{translate('All shipments which is totally closed')}}</span>
                            </div>
                            <span class="symbol symbol-light-primary symbol-45">
                                <span class="symbol-label font-weight-bolder font-size-h6">{{$delivered_shipments}}</span>
                            </span>
                        </div>
                    </div> --}}
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>

            <div class="col-xl-4 d-none"> <!-- Returned Shipments Count -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-primary card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$returned_shipments}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Returned Shipments Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-people-carry"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.shipments.index' , ['status'=>[ App\Shipment::RETURNED_CLIENT_GIVEN ] ]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-4">
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-warning card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$delivered_shipments1}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Delivered Shipments Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-box-open"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.shipments.index' , ['status'=> App\Shipment::DELIVERED_STATUS ]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-4">
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-primary card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$closed_shipments}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Closed Shipments Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-box-open"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.shipments.closed.index' , ['status'=> App\Shipment::CLOSED_STATUS   ]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-4">
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-success card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$returned_on_sender_shipments}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Returned Client Shipments Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-box-open"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.shipments.returned.sender.index' , ['status' => App\Shipment::RETURNED_STATUS  ]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
    @endif

    {{-- Admin With All Permission And Admin With Missions Index Permission  --}}
    @if($user_type == 'admin' || in_array('1100', $staff_permission) || in_array('1008', $staff_permission) )
            <div class="col-xl-6 d-none"> <!-- Total Missions Count -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-dark card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$all_missions}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Total Missions Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-shipping-fast"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.missions.index' , ['status' => 'all' , 'page_name' => translate('Total Missions Count') ]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    {{-- <div class="p-0 card-body d-flex flex-column">
                        <div onclick="location.href='{{ route('admin.missions.index' , ['status' => 'all' , 'page_name' => translate('Total Missions Count') ]) }}'" class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <a href="#" class="text-light-75 text-hover-primary font-weight-bolder font-size-h5">{{translate('Total Missions Count')}}</a>
                                <span class="mt-2 text-muted font-weight-bold">{{translate('Count all missions in the system')}}</span>
                            </div>
                            <span class="symbol symbol-light-primary symbol-45">
                                <span class="symbol-label font-weight-bolder font-size-h6">{{$all_missions}}</span>
                            </span>
                        </div>
                    </div> --}}
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-6 d-none"> <!-- Pending Missions Count -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-warning card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$pending_missions}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Pending Missions Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-shipping-fast"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.missions.index' , ['status' => [App\Mission::REQUESTED_STATUS,App\Mission::APPROVED_STATUS,App\Mission::RECIVED_STATUS] , 'page_name' => translate('Pending Missions Count')]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    {{-- <div class="p-0 card-body d-flex flex-column">
                        <div onclick="location.href='{{ route('admin.missions.index' , ['status' => [App\Mission::REQUESTED_STATUS,App\Mission::APPROVED_STATUS,App\Mission::RECIVED_STATUS] , 'page_name' => translate('Pending Missions Count')]) }}'" class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <a href="#" class="text-dark-75 font-weight-bolder font-size-h5">{{translate('Pending Missions Count')}}</a>
                                <span class="mt-2 text-dark-75 font-weight-bold">{{translate('All missions that need an action to be done')}}</span>
                            </div>
                            <span class="symbol symbol-light-primary symbol-45">
                                <span class="symbol-label font-weight-bolder font-size-h6">{{$pending_missions}}</span>
                            </span>
                        </div>
                    </div> --}}
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-4 d-none"> <!-- Pickup Missions Count -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-success card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$pickup_missions}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Pickup Missions Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-shipping-fast"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.missions.index' , ['type' => [App\Mission::PICKUP_TYPE] , 'page_name' => translate('Pickup Missions Count')]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    {{-- <div class="p-0 card-body d-flex flex-column">
                        <div onclick="location.href='{{ route('admin.missions.index' , ['type' => [App\Mission::PICKUP_TYPE] , 'page_name' => translate('Pickup Missions Count')]) }}'" class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <a href="#" class="text-light-75 text-hover-primary font-weight-bolder font-size-h5">{{translate('Pickup Missions Count')}}</a>
                                <span class="mt-2 text-muted font-weight-bold">{{translate('Count of pickup missions')}}</span>
                            </div>
                            <span class="symbol symbol-light-primary symbol-45">
                                <span class="symbol-label font-weight-bolder font-size-h6">{{$pickup_missions}}</span>
                            </span>
                        </div>
                    </div> --}}
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-4 d-none"> <!-- Delivery Missions Count -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-dark card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$delivery_missions}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Delivery Missions Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-shipping-fast"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.missions.index' , ['type' => [App\Mission::DELIVERY_TYPE] , 'page_name' => translate('Delivery Missions Count')]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    {{-- <div class="p-0 card-body d-flex flex-column">
                        <div onclick="location.href='{{ route('admin.missions.index' , ['type' => [App\Mission::DELIVERY_TYPE] , 'page_name' => translate('Delivery Missions Count')]) }}'" class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <a href="#" class="text-light-75 text-hover-primary font-weight-bolder font-size-h5">{{translate('Delivery Missions Count')}}</a>
                                <span class="mt-2 text-muted font-weight-bold">{{translate('Count of delivery missions')}}</span>
                            </div>
                            <span class="symbol symbol-light-primary symbol-45">
                                <span class="symbol-label font-weight-bolder font-size-h6">{{$delivery_missions}}</span>
                            </span>
                        </div>
                    </div> --}}
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-3 d-none"> <!-- Transfer Missions Count -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-warning card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$transfer_missions}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Transfer Missions Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-shipping-fast"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.missions.index' , ['type' => [App\Mission::TRANSFER_TYPE] , 'page_name' => translate('Transfer Missions Count')]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    {{-- <div class="p-0 card-body d-flex flex-column">
                        <div onclick="location.href='{{ route('admin.missions.index' , ['type' => [App\Mission::TRANSFER_TYPE] , 'page_name' => translate('Transfer Missions Count')]) }}'" class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <a href="#" class="text-light-75 text-hover-primary font-weight-bolder font-size-h5">{{translate('Transfer Missions Count')}}</a>
                                <span class="mt-2 text-muted font-weight-bold">{{translate('Count of transfer missions')}}</span>
                            </div>
                            <span class="symbol symbol-light-primary symbol-45">
                                <span class="symbol-label font-weight-bolder font-size-h6">{{$transfer_missions}}</span>
                            </span>
                        </div>
                    </div> --}}
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-3 d-none"> <!-- Supply Missions Count -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-primary card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$supply_missions}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Supply Missions Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-shipping-fast"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.missions.index' , ['type' => [App\Mission::SUPPLY_TYPE] , 'page_name' => translate('Supply Missions Count')]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    {{-- <div class="p-0 card-body d-flex flex-column">
                        <div onclick="location.href='{{ route('admin.missions.index' , ['type' => [App\Mission::SUPPLY_TYPE] , 'page_name' => translate('Supply Missions Count')]) }}'" class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <a href="#" class="text-light-75 text-hover-primary font-weight-bolder font-size-h5">{{translate('Supply Missions Count')}}</a>
                                <span class="mt-2 text-muted font-weight-bold">{{translate('Count of supply missions')}}</span>
                            </div>
                            <span class="symbol symbol-light-primary symbol-45">
                                <span class="symbol-label font-weight-bolder font-size-h6">{{$supply_missions}}</span>
                            </span>
                        </div>
                    </div> --}}
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-4"> <!-- Assigned & Approved Missions Count -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-warning card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$approved_missions}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Requested Missions Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-shipping-fast"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.missions.approved.index', ['status'=>App\Mission::REQUESTED_STATUS]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-4"> <!-- Recived Missions Count -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-success card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$recived_missions}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Recived Missions Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-shipping-fast"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.missions.recived.index', ['status'=>App\Mission::RECIVED_STATUS]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-4"> <!-- Done Missions Count -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-dark card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$done_missions}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Done Missions Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-shipping-fast"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.missions.done.index', ['status'=>App\Mission::DONE_STATUS]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>
            <div class="col-xl-4"> <!-- Done Missions Count -->
                <!--begin::Stats Widget 30-->
                <div class="card card-custom bg-primary card-stretch gutter-b dashcard">
                    <!--begin::Body-->
                    <div class="p-0 card-body d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center justify-content-between card-spacer">
                            <div class="mr-2 d-flex flex-column">
                                <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$closed_missions}}</span>
                                <span class="mt-2 font-weight-bolder">{{translate('Closed Missions Count')}}</span>
                            </div>
                            <i class="card-icon fas fa-shipping-fast"></i>
                        </div>
                        <div onclick="location.href='{{ route('admin.missions.done.index', ['status'=>App\Mission::CLOSED_STATUS]) }}'" class="card-more-info font-weight-bolder">
                            {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Stats Widget 30-->
            </div>

    @endif
    </div>

@elseif($user_type == 'branch')
    @php
        $all_shipments       = App\Shipment::where('branch_id', Auth::user()->userBranch->branch_id)->count();
        $pending_shipments   = App\Shipment::where('branch_id', Auth::user()->userBranch->branch_id)->whereIn('status_id', [App\Shipment::REQUESTED_STATUS, App\Shipment::CAPTAIN_ASSIGNED_STATUS, App\Shipment::RECIVED_STATUS, App\Shipment::RETURNED_STOCK])->count();
        $delivered_shipments = App\Shipment::where('branch_id', Auth::user()->userBranch->branch_id)->whereIn('status_id', [App\Shipment::DELIVERED_STATUS, App\Shipment::SUPPLIED_STATUS, App\Shipment::RETURNED_CLIENT_GIVEN])->count();

    @endphp

    <div class="row">
        <div class="col-xl-4">
            <!--begin::Stats Widget 30-->
            <div class="card card-custom bg-warning card-stretch gutter-b dashcard">
                <!--begin::Body-->
                <div class="p-0 card-body d-flex flex-column justify-content-between">
                    <div class="d-flex align-items-center justify-content-between card-spacer">
                        <div class="mr-2 d-flex flex-column">
                            <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$all_shipments}}</span>
                            <span class="mt-2 font-weight-bolder">{{translate('Total Shipments Count')}}</span>
                        </div>
                        <i class="card-icon fas fa-box-open"></i>
                    </div>
                    <div onclick="location.href='{{ route('admin.shipments.index') }}'" class="card-more-info font-weight-bolder">
                        {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                    </div>
                </div>
                {{-- <div class="p-0 card-body d-flex flex-column">
                    <div class="d-flex align-items-center justify-content-between card-spacer">
                        <div onclick="location.href='{{ route('admin.shipments.index' , ['branch_id' => Auth::user()->userBranch->branch_id]) }}'" class="mr-2 d-flex flex-column">
                            <a href="#" class="text-light-75 text-hover-primary font-weight-bolder font-size-h5">{{translate('Total Shipments Count')}}</a>
                            <span class="mt-2 text-muted font-weight-bold">{{translate('Count all shipments in the system')}}</span>
                        </div>
                        <span class="symbol symbol-light-primary symbol-45">
                            <span class="symbol-label font-weight-bolder font-size-h6">{{$all_shipments}}</span>
                        </span>
                    </div>
                </div> --}}
                <!--end::Body-->
            </div>
            <!--end::Stats Widget 30-->
        </div>
        <div class="col-xl-4">
            <!--begin::Stats Widget 30-->
            <div class="card card-custom bg-primary card-stretch gutter-b dashcard">
                <!--begin::Body-->
                <div class="p-0 card-body d-flex flex-column justify-content-between">
                    <div class="d-flex align-items-center justify-content-between card-spacer">
                        <div class="mr-2 d-flex flex-column">
                            <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$pending_shipments}}</span>
                            <span class="mt-2 font-weight-bolder">{{translate('Pending Shipments Count')}}</span>
                        </div>
                        <i class="card-icon fas fa-box-open"></i>
                    </div>
                    <div onclick="location.href='{{ route('admin.shipments.index' , ['status'=>[ App\Shipment::REQUESTED_STATUS,App\Shipment::CAPTAIN_ASSIGNED_STATUS,App\Shipment::RETURNED_STOCK,App\Shipment::RECIVED_STATUS ] ]) }}'" class="card-more-info font-weight-bolder">
                        {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                    </div>
                </div>
                {{-- <div class="p-0 card-body d-flex flex-column">
                    <div class="d-flex align-items-center justify-content-between card-spacer">
                        <div onclick="location.href='{{ route('admin.shipments.index' , ['status'=>[ App\Shipment::REQUESTED_STATUS,App\Shipment::CAPTAIN_ASSIGNED_STATUS,App\Shipment::RETURNED_STOCK,App\Shipment::RECIVED_STATUS ] , 'branch_id' => Auth::user()->userBranch->branch_id]) }}'" class="mr-2 d-flex flex-column">
                            <a href="#" class="text-dark-75 font-weight-bolder font-size-h5">{{translate('Pending Shipments Count')}}</a>
                            <span class="mt-2 text-dark-75 font-weight-bold">{{translate('All shipments that need an action to be closed')}}</span>
                        </div>
                        <span class="symbol symbol-light-primary symbol-45">
                            <span class="symbol-label font-weight-bolder font-size-h6">{{$pending_shipments}}</span>
                        </span>
                    </div>
                </div> --}}
                <!--end::Body-->
            </div>
            <!--end::Stats Widget 30-->
        </div>
        <div class="col-xl-4">
            <!--begin::Stats Widget 30-->
            <div class="card card-custom bg-success card-stretch gutter-b dashcard">
                <!--begin::Body-->
                <div class="p-0 card-body d-flex flex-column justify-content-between">
                    <div class="d-flex align-items-center justify-content-between card-spacer">
                        <div class="mr-2 d-flex flex-column">
                            <span class="text-light-75 text-hover-primary font-weight-bolder font-size-h2">{{$delivered_shipments}}</span>
                            <span class="mt-2 font-weight-bolder">{{translate('Delivered Shipments Count')}}</span>
                        </div>
                        <i class="card-icon fas fa-box-open"></i>
                    </div>
                    <div onclick="location.href='{{ route('admin.shipments.index' , ['status'=>[ App\Shipment::RETURNED_CLIENT_GIVEN,App\Shipment::SUPPLIED_STATUS,App\Shipment::DELIVERED_STATUS ] ]) }}'" class="card-more-info font-weight-bolder">
                        {{translate('View More')}} <i class="fas fa-arrow-circle-right"></i>
                    </div>
                </div>
                {{-- <div class="p-0 card-body d-flex flex-column">
                    <div class="d-flex align-items-center justify-content-between card-spacer">
                        <div onclick="location.href='{{ route('admin.shipments.index' , ['status'=>[ App\Shipment::RETURNED_CLIENT_GIVEN,App\Shipment::SUPPLIED_STATUS,App\Shipment::DELIVERED_STATUS ] , 'branch_id' => Auth::user()->userBranch->branch_id ]) }}'" class="mr-2 d-flex flex-column">
                            <a href="#" class="text-dark-75 font-weight-bolder font-size-h5">{{translate('Delivered Shipments Count')}}</a>
                            <span class="mt-2 text-dark-75 font-weight-bold">{{translate('All shipments which is totally closed')}}</span>
                        </div>
                        <span class="symbol symbol-light-primary symbol-45">
                            <span class="symbol-label font-weight-bolder font-size-h6">{{$delivered_shipments}}</span>
                        </span>
                    </div>
                </div> --}}
                <!--end::Body-->
            </div>
            <!--end::Stats Widget 30-->
        </div>

    </div>

@elseif($user_type == 'customer')
    @php
        $all_client_shipments          = App\Shipment::where('client_id', Auth::user()->userClient->client_id)->count();
        $saved_client_shipments        = App\Shipment::where('client_id', Auth::user()->userClient->client_id)->where('status_id', App\Shipment::SAVED_STATUS)->count();
        $in_progress_client_shipments  = App\Shipment::where('client_id', Auth::user()->userClient->client_id)->where('client_status', App\Shipment::CLIENT_STATUS_IN_PROCESSING)->count();
        $delivered_client_shipments    = App\Shipment::where('client_id', Auth::user()->userClient->client_id)->where('client_status', App\Shipment::CLIENT_STATUS_DELIVERED)->count();

        $transactions                   = App\Transaction::where('client_id', Auth::user()->userClient->client_id)->orderBy('created_at','desc')->sum('value');
    @endphp

    <div class="row">
        <div class="col-xl-12">
            <!--begin::Stats Widget 30-->
            <div class="card card-custom bgi-no-repeat card-stretch gutter-b dashcard">
                <!--begin::Body-->
                <div class="card-body">
                    <a href="#" class="card-title font-weight-bold text-muted text-hover-primary font-size-h5">{{translate('Your Wallet')}}</a>
                    <div class="mb-5 font-weight-bold text-success mt-9">{{format_price($transactions)}}</div>
                    <p class="m-0 text-dark-75 font-weight-bolder font-size-h5">{{translate('The amount you have on your wallet, Which you can request anytime')}}.</p>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Stats Widget 30-->
        </div>
    </div>

    <div class="row">
        <div class="col-xl-3">
            <!--begin::Stats Widget 30-->
            <div class="card card-custom bg-dark card-stretch gutter-b dashcard">
                <!--begin::Body-->
                <div class="p-0 card-body d-flex flex-column">
                    <div class="d-flex align-items-center justify-content-between card-spacer">
                        <div onclick="location.href='{{ route('admin.shipments.index' , ['client_id' => Auth::user()->userClient->client_id ]) }}'" class="mr-2 d-flex flex-column">
                            <a href="#" class="text-light-75 text-hover-primary font-weight-bolder font-size-h5">{{translate('Total Shipments Count')}}</a>
                            <span class="mt-2 text-muted font-weight-bold">{{translate('Count all shipments you have')}}</span>
                        </div>
                        <span class="symbol symbol-light-primary symbol-45">
                            <span class="symbol-label font-weight-bolder font-size-h6">{{$all_client_shipments}}</span>
                        </span>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Stats Widget 30-->
        </div>
        <div class="col-xl-3">
            <!--begin::Stats Widget 30-->
            <div class="card card-custom bg-dark card-stretch gutter-b dashcard">
                <!--begin::Body-->
                <div class="p-0 card-body d-flex flex-column">
                    <div class="d-flex align-items-center justify-content-between card-spacer">
                        <div onclick="location.href='{{ route('admin.shipments.index' , ['status'=>[ App\Shipment::SAVED_STATUS] , 'client_id' => Auth::user()->userClient->client_id ]) }}'" class="mr-2 d-flex flex-column">
                            <a href="#" class="text-light-75 text-hover-primary font-weight-bolder font-size-h5">{{translate('Saved Shipments Count')}}</a>
                            <span class="mt-2 text-muted font-weight-bold">{{translate('Count of your saved shipments')}}</span>
                        </div>
                        <span class="symbol symbol-light-primary symbol-45">
                            <span class="symbol-label font-weight-bolder font-size-h6">{{$saved_client_shipments}}</span>
                        </span>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Stats Widget 30-->
        </div>
        <div class="col-xl-3">
            <!--begin::Stats Widget 30-->
            <div class="card card-custom bg-dark card-stretch gutter-b dashcard">
                <!--begin::Body-->
                <div class="p-0 card-body d-flex flex-column">
                    <div class="d-flex align-items-center justify-content-between card-spacer">
                        <div onclick="location.href='{{ route('admin.shipments.index' , ['client_status'=>[ App\Shipment::CLIENT_STATUS_IN_PROCESSING] , 'client_id' => Auth::user()->userClient->client_id ]) }}'" class="mr-2 d-flex flex-column">
                            <a href="#" class="text-light-75 text-hover-primary font-weight-bolder font-size-h5">{{translate('In Progress Shipments Count')}}</a>
                            <span class="mt-2 text-muted font-weight-bold">{{translate('Count of your shipments which is in the shipping process')}}</span>
                        </div>
                        <span class="symbol symbol-light-primary symbol-45">
                            <span class="symbol-label font-weight-bolder font-size-h6">{{$in_progress_client_shipments}}</span>
                        </span>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Stats Widget 30-->
        </div>
        <div class="col-xl-3">
            <!--begin::Stats Widget 30-->
            <div class="card card-custom bg-dark card-stretch gutter-b dashcard">
                <!--begin::Body-->
                <div class="p-0 card-body d-flex flex-column">
                    <div class="d-flex align-items-center justify-content-between card-spacer">
                        <div onclick="location.href='{{ route('admin.shipments.index' , ['client_status'=>[ App\Shipment::CLIENT_STATUS_DELIVERED] , 'client_id' => Auth::user()->userClient->client_id ]) }}'" class="mr-2 d-flex flex-column">
                            <a href="#" class="text-light-75 text-hover-primary font-weight-bolder font-size-h5">{{translate('Delivered Shipments Count')}}</a>
                            <span class="mt-2 text-muted font-weight-bold">{{translate('Count of your delivered shipments')}}</span>
                        </div>
                        <span class="symbol symbol-light-primary symbol-45">
                            <span class="symbol-label font-weight-bolder font-size-h6">{{$delivered_client_shipments}}</span>
                        </span>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Stats Widget 30-->
        </div>
    </div>
@elseif($user_type == 'captain')
    @php
        $transactions = App\Transaction::where('captain_id', Auth::user()->userCaptain->captain_id)->orderBy('created_at','desc')->sum('value');
        //$transactions = abs($transactions); // Converting the transactions from negative to positive
    @endphp

    <div class="row">
        <div class="col-xl-12">
            <!--begin::Stats Widget 30-->
            <div class="card card-custom bg-dark card-stretch gutter-b dashcard">
                <!--begin::Body-->
                <div class="card-body">
                    <a href="#" class="mb-0 card-title font-weight-bold text-light-75 text-hover-primary font-size-h5">{{translate('Your Wallet')}}</a>
                    <div class="mt-0 mb-5 font-weight-bold font-size-h4 text-success mt-9">{{format_price($transactions)}}</div>
                    <p class="m-0 text-muted font-weight-bolder font-size-h5">{{translate('The amount you have on your wallet, Which you should deliver to customer or company')}}.</p>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Stats Widget 30-->
        </div>
    </div>
@endif
