@php 
$addon = \App\Addon::where('unique_identifier', 'spot-cargo-shipment-addon')->first();
@endphp
@if ($addon != null)
    @if($addon->activated)
        @if(in_array(Auth::user()->user_type , ['admin','customer','branch']) || in_array('1001', json_decode(Auth::user()->staff->role->permissions ?? "[]")) || Auth::user()->user_type == 'branch' || Auth::user()->user_type == 'client')
            <li class="menu-item menu-item-rel ">
                <a href="{{ route('admin.shipments.create') }}" class="btn btn-success btn-md mr-3">
                    + {{translate('Add Shipment')}}<i class="ml-2 flaticon2-box-1"></i>
                </a>
            </li>
        @endif
        <li class="menu-item menu-item-rel ">
            <form action="{{route('admin.shipments.trackingFromDashboard')}}" class="form-default" role="form" method="POST" enctype="multipart/form-data">
                @csrf
            
                <div class="d-flex align-items-center">
                    <input type="text" class="form-control" placeholder="Ex: AWB123456" name="code" id="code"/>
                    <button type="submit" class="btn btn-success btn-md ml-2">
                        <i class="ml-2 flaticon2-search"></i>
                    </button>
                </div>
            </form>
            {{-- <a href="{{ route('admin.shipments.track') }}" class="btn btn-primary btn-sm mr-3">
                {{translate('Track Shipment')}}<i class="ml-2 flaticon2-search"></i>
            </a> --}}
        </li>
        @if(Auth::user()->user_type == 'admin')
            <li class="menu-item menu-item-rel ">
                <button type="button" class="btn btn-success btn-sm"
                    data-toggle="modal" data-target="#endOfDay"
                    title="{{ translate('End Of Day') }}">
                    {{ translate('End Of Day') }}
                </button>
        
            </li>
        @endif
    @endif
 
@endif
